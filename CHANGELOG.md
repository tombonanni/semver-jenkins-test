# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.4.0](https://bitbucket.org/tombonanni/semver-jenkins-test/compare/v1.3.0...v1.4.0) (2020-05-04)


### Features

* **index.js:** added test2 ([08bab81](https://bitbucket.org/tombonanni/semver-jenkins-test/commit/08bab81581b8bf57072cb329ab8161350228550c))

## [1.3.0](https://bitbucket.org/tombonanni/semver-jenkins-test/compare/v1.2.0...v1.3.0) (2020-05-04)

### Features

- **index.js:** added test 3 feature ([68b1da9](https://bitbucket.org/tombonanni/semver-jenkins-test/commit/68b1da93d3320d5653cbd5ae1b94ca274d29137d))
- **index.js:** added test feature ([08a4b8d](https://bitbucket.org/tombonanni/semver-jenkins-test/commit/08a4b8d43409a93efc78852b09a29b4a59aa0c92))

## [1.2.0](https://bitbucket.org/tombonanni/semver-jenkins-test/compare/v1.1.0...v1.2.0) (2020-05-04)

### Features

- **index.js:** added other func ([fe59832](https://bitbucket.org/tombonanni/semver-jenkins-test/commit/fe5983216f1c7455a909c8333253af82ed0bb359))

## [1.1.0](https://bitbucket.org/tombonanni/semver-jenkins-test/compare/v1.0.0...v1.1.0) (2020-05-04)

### Features

- **index.js:** added the main function ([b16d5ab](https://bitbucket.org/tombonanni/semver-jenkins-test/commit/b16d5ab6e0cfe5776c8a13c8f5663daceca94444))

## 1.0.0 (2020-05-04)

### Features

- **package.json:** added git-cz ([f959fb5](https://bitbucket.org/tombonanni/semver-jenkins-test/commit/f959fb596254065463f2db95761ac2f1ce28e3de))
